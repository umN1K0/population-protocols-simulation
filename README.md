# Population Protocols Simulation

This project in the part Bachelor's thesis "Probabilistic Population Protocol Models" by Vladyslav Melnychuk. 

It contains the implementation of a population protocol for leader election due to Angluin et al. as well as its improved version (see <mark>src</mark>). We also provide several related simulations and their results. The results can be visualized with the help of <mark>Simulations.ipynb</mark>.  

import java.io.*;
import java.util.*;

public class LeaderElection {

    Agent[] agents;
    int populationSize;
    int leaderCount;

    public LeaderElection(int numOfAgents){
        this.agents = new Agent[numOfAgents];
        this.populationSize = numOfAgents;
        this.leaderCount = numOfAgents;
        for (int i = 0; i < numOfAgents; i++){
            agents[i] = new Agent(1,0, 0 , 0, 0, 0);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(agents);
    }

    // Simulation 1: Leader Election by Angluin et al.
    // n vs. maxK vs. number of leaders
    public static int[] simulation1(int numOfAgents){
        LeaderElection le = new LeaderElection(numOfAgents);
        int largestK = 0;
        HashSet<Agent> reachedLargestK = new HashSet<>();
        HashSet<Integer> leaders = new HashSet<>();
        for (int i = 0; i < numOfAgents; i++){
            leaders.add(i);
        }

        while (le.leaderCount != 1) {
            Random random = new Random();
            Integer[] currLeaders = leaders.toArray(new Integer[leaders.size()]);
            int i = currLeaders[random.nextInt(currLeaders.length)];
            int j = random.nextInt(le.populationSize);
            while (i == j) {
                j = random.nextInt(le.populationSize);
            }
            Agent agent1 = le.agents[i];
            Agent agent2 = le.agents[j];

            // 0) first, try to reset counters if needed
            resetCounters(agent1, agent2);

            // if at least one of the agents still needs to reset a counter, we are done
            if (agent1.timerReset == 0 && agent2.timerReset == 0) {
                // 1) two leaders
                if(agent1.leader == 1 && agent2.leader == 1){
                    leaderAndLeader(agent1, agent2, le);
                    leaders.remove(j);
                }
                // 2.1) leader and non-leader
                 else if (agent1.leader == 1){
                    largestK = leaderAndNonLeader(agent1, agent2, largestK, reachedLargestK);
                }
                // 2.2) non-leader and leader
                else if (agent2.leader == 1){
                    largestK = leaderAndNonLeader(agent2, agent1, largestK, reachedLargestK);
                }
            }
        }
        return new int[]{largestK, reachedLargestK.size()};
    }

    //Simulation 2: fix K, look at the final number of leaders
    public static int simulation2(int numOfAgents, int maxK){
        LeaderElection le = new LeaderElection(numOfAgents);
        int largestK = 0;
        HashSet<Agent> reachedLargestK = new HashSet<>();
        while (largestK != maxK && le.leaderCount != 1) {

            Random random = new Random();
            int i = random.nextInt(le.populationSize);
            int j = random.nextInt(le.populationSize);
            while (i == j) {
                j = random.nextInt(le.populationSize);
            }
            Agent agent1 = le.agents[i];
            Agent agent2 = le.agents[j];

            // 0) first, try to reset counters if needed
            resetCounters(agent1, agent2);

            // if at least one of the agents still needs to reset a counter, we are done
            if (agent1.timerReset == 0 && agent2.timerReset == 0) {
                // 1) two leaders
                if(agent1.leader == 1 && agent2.leader == 1){
                    leaderAndLeader(agent1, agent2, le);
                }
                // 2.1) leader and non-leader
                else if (agent1.leader == 1){
                    largestK = leaderAndNonLeader(agent1, agent2, largestK, reachedLargestK);
                }
                // 2.2) non-leader and leader
                else if (agent2.leader == 1){
                    largestK = leaderAndNonLeader(agent2, agent1, largestK, reachedLargestK);
                }
            }
        }
        return le.leaderCount;
    }

    // Simulation 3: Simulation 2 for the improved version of protocol
    public static int simulation3(int numOfAgents, int maxK){
        LeaderElection le = new LeaderElection(numOfAgents);
        int largestK = 0;
        HashSet<Agent> reachedLargestK = new HashSet<>();
        while (largestK != maxK && le.leaderCount != 1){
            Random random = new Random();
            int i = random.nextInt(le.populationSize);
            int j = random.nextInt(le.populationSize);
            while (i == j) {
                j = random.nextInt(le.populationSize);
            }
            Agent agent1 = le.agents[i];
            Agent agent2 = le.agents[j];

            // 0) first, try to reset counters if needed
            resetCounters(agent1, agent2);
            // 1) two leaders
            if(agent1.leader == 1 && agent2.leader == 1){
                leaderAndLeaderImproved(agent1, agent2, le);
            }
            // 2.1) leader and non-leader
            else if (agent1.leader == 1){
                largestK = leaderAndNonLeaderImproved(agent1, agent2, largestK, reachedLargestK, le);
            }
            // 2.2) non-leader and leader
            else if (agent2.leader == 1){
                largestK = leaderAndNonLeaderImproved(agent2, agent1, largestK, reachedLargestK, le);
            }
            //Improvement: two timers bumping into each other delete one of the timer bits
            else if (agent1.timer == 1 && agent2.timer == 1){
                agent2.timer = 0;
                agent2.hasSeenTimer = 1;
            }
            //Improvement: propagating of the hasSeenTimer mark
            else if (agent1.timer == 1){
                agent2.hasSeenTimer = 1;
            } else if (agent2.timer == 1){
                agent1.hasSeenTimer = 1;
            } else if (agent1.hasSeenTimer == 1 || agent2.hasSeenTimer == 1){
                agent1.hasSeenTimer = 1;
                agent2.hasSeenTimer = 1;
            }
        }
        return le.leaderCount;
    }


    private static void resetCounters(Agent agent1, Agent agent2) {
        if (agent1.timerReset == 1 && agent2.timer == 1) {
            agent1.timerReset = 0;
            agent2.timer = 0;
        }
        if (agent2.timerReset == 1 && agent1.timer == 1) {
            agent2.timerReset = 0;
            agent1.timer = 0;
        }
    }

    public static void leaderAndLeader(Agent agent1, Agent agent2, LeaderElection le){
        agent2.leader = 0;
        if(agent2.timerSet == 1){
            agent2.timerSet = 0;
            // do the timer reset immediately, if possible
            if (agent1.timer == 1){
                agent1.timer = 0;
            } else {
                agent1.timerReset = 1;
            }
        }
        agent1.timerCount = 0;
        le.leaderCount--;
    }

    public static int leaderAndNonLeader(Agent agent1, Agent agent2, int largestK, HashSet<Agent> reachedLargestK){

        int updatedLargestK = largestK;

        // marking the first non-timer agent as a timer
        updatedLargestK = getUpdatedLargestK(agent1, agent2, largestK, reachedLargestK, updatedLargestK);

        return updatedLargestK;
    }

    private static int getUpdatedLargestK(Agent agent1, Agent agent2, int largestK, HashSet<Agent> reachedLargestK, int updatedLargestK) {
        if (agent1.timerSet == 0 && agent2.timer == 0){
            agent1.timerSet = 1;
            agent2.timer = 1;
        }
        // counting consecutive interactions with timers
        else if (agent1.timerSet == 1 && agent2.timer == 1){
            agent1.timerCount++;
            if (agent1.timerCount == largestK){
                reachedLargestK.add(agent1);
            } else if (agent1.timerCount > largestK){
                updatedLargestK++;
                reachedLargestK.clear();
                reachedLargestK.add(agent1);
            }
        } else{
            agent1.timerCount = 0;
        }
        return updatedLargestK;
    }

    public static void leaderAndLeaderImproved(Agent agent1, Agent agent2, LeaderElection le){
        agent2.leader = 0;
        if(agent2.timerSet == 1){
            agent2.timerSet = 0;
            // do the timer reset immediately, if possible
            if (agent1.timer == 1){
                agent1.timer = 0;
            } else {
                agent1.timerReset = 1;
            }
        }
        //Improvement: loser gets a has-seen-timer mark if winner has set a timer
        if(agent1.timerSet == 1){
            agent2.hasSeenTimer = 1;
        }
        agent1.timerCount = 0;
        le.leaderCount--;
    }

    public static int leaderAndNonLeaderImproved(Agent agent1, Agent agent2, int largestK, HashSet<Agent> reachedLargestK, LeaderElection le){

        int updatedLargestK = largestK;

        //Improvement: a has-seen-timer-agent initializes the leader who did not set the timer yet
        if (agent1.timerSet == 0 && (agent2.hasSeenTimer == 1 || agent2.timer == 1)){
            agent1.leader = 0;
            agent1.timer = 0;
            le.leaderCount--;
            agent1.hasSeenTimer = 1;
        }
        // marking the first non-timer agent as a timer
        else updatedLargestK = getUpdatedLargestK(agent1, agent2, largestK, reachedLargestK, updatedLargestK);

        return updatedLargestK;
    }

    public static void main(String[] args) throws IOException {
        // Uncomment to run a simulation
        // runSimulation1();
        // runSimulation2();
        // runSimulation3();
    }

    public static void runSimulation1() throws IOException {
        File fout = new File("src/Simulation_Results/Simulation_1_results.txt");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        int testSize = 50000;
        for (int i =10; i < testSize; i+=50){
            int[] res = simulation1(i);
            bw.write(i+" "+res[0]+" "+res[1]);
            bw.newLine();
            System.out.println(i+" "+res[0]+" "+res[1]);
        }
        bw.close();
    }

    public static void runSimulation2() throws IOException {
        File fout = new File("src/Simulation_Results/Simulation_2_results.txt");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        int testSize = 300000;
        for (int i =100; i < testSize; i+=50){
            int res = simulation2(i, 4);
            bw.write(i+" "+res);
            bw.newLine();
            System.out.println(i+" "+res);
        }
        bw.close();
    }

    public static void runSimulation3() throws IOException {
        File fout = new File("src/Simulation_Results/Simulation_3_results.txt");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        int testSize = 300000;
        for (int i =100; i < testSize; i+=50){
            int res = simulation3(i, 4);
            bw.write(i+" "+res);
            bw.newLine();
            System.out.println(i+" "+res);
        }
        bw.close();
    }

}

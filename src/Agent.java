public class Agent {

    public int leader;          // 1 if leader bit is set, 0 otherwise
    public int timer;           // 1 if timer bit is set, 0 otherwise
    public int timerSet;        // 1 if agent has marked a timer, 0 otherwise
    public int timerReset;      // 1 if agent needs to reset a timer before proceeding with initialization, 0 otherwise
    public int timerCount;      // number of consecutive interactions with timers
    public int hasSeenTimer;    // 1 if agent has seen a timer agent

    public Agent(int leader, int timer, int timerSet, int timerReset, int timerCount, int hasSeenTimer){
        this.leader = leader;
        this.timer = timer;
        this.timerSet = timerSet;
        this.timerReset = timerReset;
        this.timerCount = timerCount;
        this.hasSeenTimer = hasSeenTimer;
    }

    @Override
    public String toString() {
        return "["+leader+","+timer+","+timerSet+","+timerReset+","+timerCount+","+hasSeenTimer+"]";
    }
}
